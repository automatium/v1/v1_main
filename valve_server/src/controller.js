const fs = require("fs");
const path = require("path");
const chalk = require("chalk");
const SerialPort = require("serialport");
const Buffer = require("buffer");
const log = require("ololog");
const es = require("event-stream");
const Koa = require("koa");
const Router = require("koa-router");
const logger = require("koa-logger");
const PQueue = require("p-queue");
const serve = require("koa-static");

const app = new Koa();
const router = new Router();

const cmdQueue = new PQueue({ concurrency: 1 });

// serial port settings
const SERIAL_PORT = process.env.SERIAL_PORT || "/dev/cu.usbmodem1D1120";
const SERIAL_BAUD = 115200;

// how many bytes of data to send in each wireless packet
const CHUNK_SIZE = 200;

// logger
app.use(logger());

// middleware
app.use(serve("./public"));

// routes
router.get("/send/:device/:msg", async ctx => {
  logAction(
    `Processing command to send ${ctx.params.msg} to ${ctx.params.device}`
  );

  if (process.env.MOCK_SERIAL) {
    ctx.status = 200;
    return;
  }

  await cmdQueue.add(() =>
    sendPacket(Number(ctx.params.device), ctx.params.msg)
  );
  ctx.status = 200;
});

// request handler
app.use(router.routes());
app.use(router.allowedMethods());

// start the server
const listenPort = process.env.PORT || 4000;
logAction(`Listening on port *:${listenPort}`);
app.listen(listenPort);

async function sendPacket(device, data) {
  logAction("Opening serial connection");
  const port = new SerialPort(SERIAL_PORT, {
    baudRate: SERIAL_BAUD
  });

  // always exit cleanly
  const handleSigint = () => {
    console.log("Gracefully closing the serial port");

    port.close(() => {
      process.exit();
    });
  };
  process.on("SIGINT", handleSigint);

  // log output from the port
  port.on("data", data => {
    let output = data.toString().trim();

    if (output.length) {
      output = output.replace("\n", "\n<- ");
      console.log("<-", output);
    }
  });

  try {
    // wait for the device to wake up
    logAction("Waiting for device");
    await write(port, "STATUS");
    await streamWait(port, "1", "0");

    // actually send the packet
    logAction(`Sending ${data} to ${device}`);
    await sendPacketToPort(port, device, data);

    console.log(`Packet sent successfully!`);
    process.removeListener("SIGINT", handleSigint);
  } catch (e) {
    console.log("Error sending packet", e);
    throw e;
  } finally {
    logAction("Closing the serial port.");
    await closePort(port);
  }
}

async function closePort(port) {
  return new Promise((resolve, reject) => {
    port.close(err => {
      if (err) {
        reject();
      } else {
        resolve();
      }
    });
  });
}

async function sendPacketToPort(port, device, data) {
  // enter input mode
  await writeWait(port, "MSG_START" + String.fromCharCode(device), "READY");

  // watch for input errors, but don't hold up the process
  streamWait(port, undefined, "INPUT_ERR", false);

  // write the packet body
  await write(port, data);

  // end the packet
  await writeWait(port, "MSG_SEND", "SEND_OK", "SEND_ERR", 60000);
}

function writeWait(port, data, ...params) {
  return Promise.all([streamWait(port, ...params), write(port, data)]).then(
    r => r[0]
  );
}

function write(port, data) {
  return new Promise((resolve, reject) => {
    console.log("->", data);

    port.write(data, err => {
      if (err) reject(err);
      else resolve();
    });
  });
}

function streamWait(
  port,
  searchStrings = [],
  errStrings = [],
  timeout = 10000
) {
  return new Promise((resolve, reject) => {
    if (typeof searchStrings === "string") {
      searchStrings = [searchStrings];
    }
    if (typeof errStrings === "string") {
      errStrings = [errStrings];
    }

    let timer;
    if (timeout !== false) {
      timer = setTimeout(() => {
        port.removeListener("data", cc);
        reject(
          "No response from serial port!  Expecting one of: " +
            // $FlowFixMe
            searchStrings.join(", ")
        );
      }, timeout);
    }

    let allMatches = new Map([
      ...searchStrings.map(string => {
        return [string, []];
      }),
      ...errStrings.map(string => {
        return [string, []];
      })
    ]);

    function cc(chunk) {
      // console.log('Processing chunk', chunk.toString())
      for (const char of chunk.toString().trim()) {
        // console.log('processing char', char, char.charCodeAt(0))
        allMatches.forEach((matches, string) => {
          // called when we've found a match
          const cleanup = () => {
            // console.log(allMatches, 'Done!')
            port.removeListener("data", cc);
            clearTimeout(timer);
            if (errStrings.includes(string)) {
              reject(`Got error from device: ${string}`);
            } else {
              resolve();
            }
          };

          // new matches
          const newMatches = [];

          // check if we're at the start of a new match
          if (char === string[0]) {
            // start of a new potential match
            newMatches.push(0);

            if (string.length === 1) {
              cleanup();
            }
          }

          // process existing matches
          matches.forEach(i => {
            i++;
            if (char === string[i]) {
              // we still have a match
              // add it to the list
              newMatches.push(i);

              if (string.length - 1 === i) {
                // final match!
                cleanup();
              }
            }
          });
          allMatches.set(string, newMatches);
        });

        // console.log(allMatches)
        // console.log()
      }
    }

    port.on("data", cc);
  });
}

function logAction(...props) {
  return console.log(chalk.blue("==>", ...props));
}

function pause(delay) {
  return new Promise(resolve => {
    setTimeout(resolve, delay);
  });
}
