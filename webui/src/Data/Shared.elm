module Data.Shared exposing (..)

-- Could be a MAC address, or anything else that uniquely identifies the thing


type alias GUID =
    String



-- number of miliseconds since midnight


type alias TimeOfDay =
    Int
