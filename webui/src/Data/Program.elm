module Data.Program exposing (..)

import Data.Shared exposing (GUID)
import Data.Rule exposing (ActivationRule)


-- nothing here yet!


type alias Program =
    { name : String
    , ports : List GUID
    , rule : ActivationRule
    , priority : Int
    }
