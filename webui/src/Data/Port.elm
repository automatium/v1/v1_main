module Data.Port exposing (..)

import Data.Shared exposing (GUID)
import Time exposing (Time)


type alias IoPort =
    { guid : GUID
    , name : String
    , connection : PortConnection
    , job : Maybe Job
    , version : Int
    , notes : String
    }


type PortConnection
    = V1Connection
        { deviceId : String
        , portNumber : String
        }
    -- | LoRaWanConnection
    --     { deviceId : String
    --     , portNumber : String
    --     }


type PortType
    = DcLatchingSolenoidOutput


type alias Job =
    { duration : Time
    , started : Maybe Time
    , canceled : Bool
    }


createJob : Time -> Job
createJob duration =
    Job duration Nothing False


type IoTask
    = NoTask
    | ScheduledTask { runtime : Int }
    | RunningTask { runtime : Int, startedAt : Int }
    | ErroredTask { runtime : Int, error : String }


mockV1Port : String -> String -> IoPort
mockV1Port deviceId portNumber =
    let
        portAddr =
            deviceId ++ "." ++ portNumber
    in
        { guid = ("port" ++ portAddr)
        , name = ("Port " ++ portAddr)
        , connection = (V1Connection { deviceId = deviceId, portNumber = portNumber })
        , job = Nothing
        , version = 0
        , notes = ""
        }


makePort : String -> String -> PortConnection -> Maybe Job -> IoPort
makePort guid name connection job =
    IoPort
        guid
        name
        connection
        job
        0
        ""


blankV1Connection : PortConnection
blankV1Connection =
    V1Connection { deviceId = "", portNumber = "" }
