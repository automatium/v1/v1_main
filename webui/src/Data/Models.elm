module Data.Models exposing (..)

import Dict exposing (Dict)
import Random.Pcg exposing (Seed, initialSeed, step)
import Data.Shared exposing (GUID)
import Data.Port exposing (IoPort, mockV1Port)
import Data.Route exposing (Route(..))
import Pages.Port as Port
import Time exposing (Time)


type alias Model =
    { route : Route
    , uuidSeed : Seed
    , ports : PortsDict
    , portPage : Port.Model
    , time : Time
    }


type alias PortsDict =
    Dict GUID IoPort


type alias Node =
    { guid : GUID
    , name : String
    , modules : List GUID
    }


type alias IoModule =
    { guid : GUID
    , kind : ModuleKind
    , ports : List GUID
    }


type ModuleKind
    = ValveOutput


initialModel : Route -> Seed -> Model
initialModel route seed =
    { route = route
    , uuidSeed = seed
    , ports =
        Dict.empty
    , portPage = Port.initialModel
    , time = 0
    }



-- , ports =
--     (guidDict
--         [ (mockV1Port "1" "1")
--         , (mockV1Port "1" "2")
--         , (mockV1Port "2" "1")
--         , (mockV1Port "2" "2")
--         , (mockV1Port "3" "1")
--         , (mockV1Port "3" "2")
--         , (mockV1Port "4" "1")
--         , (mockV1Port "4" "2")
--         ]
--     )


guidDict :
    List { b | guid : comparable }
    -> Dict comparable { b | guid : comparable }
guidDict entries =
    let
        extractTuple node =
            ( node.guid, node )
    in
        Dict.fromList <| List.map extractTuple entries
