module Data.Route exposing (..)

import Data.Shared exposing (GUID)
import Navigation exposing (Location)
import UrlParser exposing (..)


type Route
    = NetworkRoute
    | PortRoute GUID
    | NotFoundRoute


matchers : Parser (Route -> a) a
matchers =
    oneOf
        [ map NetworkRoute top
        , map PortRoute (s "network" </> s "port" </> string)
        , map NetworkRoute (s "network")
        ]


parseLocation : Location -> Route
parseLocation location =
    case (parseHash matchers location) of
        Just route ->
            route

        Nothing ->
            NotFoundRoute


networkPath : String
networkPath =
    "#network"


portPath : GUID -> String
portPath guid =
    "#network/port/" ++ guid
