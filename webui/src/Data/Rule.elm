module Data.Rule exposing (..)

import Data.Shared exposing (TimeOfDay)


type ActivationRule
    = ComboRule
        { combinator : RuleCombinator
        , children : List ActivationRule
        }
    | DayOfWeek
        { sunday : Bool
        , monday : Bool
        , tuesday : Bool
        , wednesday : Bool
        , thursday : Bool
        , friday : Bool
        , saturday : Bool
        }
    | TimeOfDay
        { startTime : TimeOfDay
        , endTime : TimeOfDay
        }


type RuleCombinator
    = Any
    | All
