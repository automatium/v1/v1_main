module Views.Header exposing (..)

import Css as C exposing (..)
import Html.Styled exposing (..)


viewHeader : Html msg
viewHeader =
    headerContainer []
        [ colorBar [] []
        , headerContent []
            [ logoContainer [] [ text "Automatium" ]
            ]
        ]


headerContainer : List (Attribute msg) -> List (Html msg) -> Html msg
headerContainer =
    styled nav
        [ displayFlex
        , flexDirection column
        , color (hex "#fff")
        ]


colorBar : List (Attribute msg) -> List (Html msg) -> Html msg
colorBar =
    styled div
        [ backgroundImage (linearGradient2 toRight (stop <| hex "#085078") (stop <| hex "#85d8ce") [])
        , height (px 5)
        ]


headerContent : List (Attribute msg) -> List (Html msg) -> Html msg
headerContent =
    styled div
        [ backgroundColor (hex "#2d343a")
        , height (px 60)
        , displayFlex
        , alignItems center
        ]


logoContainer : List (Attribute msg) -> List (Html msg) -> Html msg
logoContainer =
    styled h1
        [ margin zero
        , marginLeft (C.rem 1)
        , fontWeight (int 400)
        ]
