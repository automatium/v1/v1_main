module Views.Page exposing (..)

import Css as C exposing (..)
import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (href, css)
import Data.Port exposing (IoPort)
import Data.Shared exposing (GUID)
import Dict exposing (Dict)
import Data.Route as Route exposing (Route, networkPath, portPath)
import Dict


appContainer : List (Attribute msg) -> List (Html msg) -> Html msg
appContainer =
    styled div
        [ displayFlex
        , flexDirection column
        , minHeight (pct 100)
        , backgroundColor (hex "#f9f9f9")
        ]


contentContainer : List (Attribute msg) -> List (Html msg) -> Html msg
contentContainer =
    styled div
        [ padding (C.rem 2)
        ]


breadcrumbs : Dict GUID IoPort -> Route -> Html msg
breadcrumbs ports route =
    let
        networkCrumb =
            everBlue networkPath "Network"

        nameOrGuid guid =
            case (Dict.get guid ports) of
                Just prt ->
                    prt.name

                Nothing ->
                    guid
    in
        case route of
            Route.NetworkRoute ->
                h2 [] [ networkCrumb ]

            Route.PortRoute guid ->
                h2 [] [ networkCrumb, text " / ", everBlue (portPath guid) (nameOrGuid guid) ]

            Route.NotFoundRoute ->
                h2 [] [ text "Not Found!" ]


everBlue : String -> String -> Html msg
everBlue url txt =
    a [ href url, css [ color (hex "#0070c9"), textDecoration none ] ] [ text txt ]


notFound : Html msg
notFound =
    h1 [] [ text "Not Found" ]
