module Main exposing (..)

import Navigation exposing (Location)
import Data.Route as Route
import Data.Models exposing (Model, initialModel)
import Update exposing (..)
import Msgs exposing (Msg(..))
import Random.Pcg exposing (initialSeed)
import Views.Page exposing (appContainer, contentContainer, breadcrumbs, notFound)
import Views.Header exposing (viewHeader)
import Html
import Html.Styled exposing (toUnstyled, Html)
import Pages.Network as Network
import Pages.Port as Port
import Dict
import Request.Port exposing (getPorts)
import Time exposing (second)


---- MODEL ----


init : Flags -> Location -> ( Model, Cmd Msg )
init { randomSeed } location =
    let
        currentRoute =
            Route.parseLocation location

        _ =
            Debug.log "Current Route" <| toString currentRoute
    in
        ( initialModel currentRoute (initialSeed randomSeed), getPorts GotPorts )



---- SUBSCRIPTIONS ----


subscriptions : Model -> Sub Msg
subscriptions model =
    Time.every (second * 1) Tick



---- VIEW ----


view : Model -> Html.Html Msg
view model =
    toUnstyled <| app model


app : Model -> Html Msg
app model =
    appContainer []
        [ viewHeader
        , contentContainer []
            [ breadcrumbs model.ports model.route
            , page model
            ]
        ]


page : Model -> Html Msg
page model =
    case model.route of
        Route.NetworkRoute ->
            Network.view model.ports

        Route.PortRoute guid ->
            case (Dict.get guid model.ports) of
                Just prt ->
                    Port.view model.time model.portPage prt |> Html.Styled.map UpdatePort

                Nothing ->
                    notFound

        Route.NotFoundRoute ->
            notFound



---- program  ----


main : Program Flags Model Msg
main =
    Navigation.programWithFlags Msgs.OnLocationChange
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


type alias Flags =
    { randomSeed : Int
    }
