module Pages.Port exposing
    ( ConnectionMsg(..)
    , Model
    , Msg(..)
    , divRest
    , floatField
    , formSection
    , incDecFloatField
    , initialModel
    , intField
    , paddedButton
    , portContainer
    , portNetworkParams
    , radioField
    , textArea
    , textField
    , toClock
    , toTime
    , toTimeComponents
    , update
    , updateConnection
    , view
    )

import Basics as B
import Css as C exposing (..)
import Data.Port exposing (IoPort, PortConnection(..), createJob)
import Html.Styled as Html exposing (..)
import Html.Styled.Attributes as A exposing (checked, css, selected, type_, value)
import Html.Styled.Events exposing (onClick, onInput)
import Html.Styled.Lazy exposing (lazy)
import String exposing (fromChar)
import Time exposing (Time, hour, minute, second)



---- View ----


view : Time -> Model -> IoPort -> Html Msg
view time model prt =
    portContainer []
        [ formSection "Run"
            (case prt.job of
                Nothing ->
                    [ incDecFloatField "Hours" model.hours UpdateHours
                    , incDecFloatField "Minutes" model.minutes UpdateMinutes
                    , incDecFloatField "Seconds" model.seconds UpdateSeconds
                    , paddedButton
                        [ onClick CreateJob
                        ]
                        [ toTime model
                            |> toClock
                            |> (++) "Run for "
                            |> text
                        ]
                    ]

                Just job ->
                    case job.canceled of
                        True ->
                            [ div [] [ text <| "Stopping job for " ++ toClock job.duration ]
                            , paddedButton [ onClick KillJob ] [ text "Kill Job" ]
                            ]

                        False ->
                            case job.started of
                                Nothing ->
                                    [ div [] [ text <| "Starting job for " ++ toClock job.duration ]
                                    , button [ onClick CancelJob ] [ text "Cancel Job" ]
                                    ]

                                Just started ->
                                    let
                                        elapsed =
                                            time - started

                                        remaining =
                                            job.duration - elapsed
                                    in
                                    if elapsed < job.duration then
                                        [ div [] [ text <| "Running job for " ++ toClock job.duration ++ "." ]
                                        , div [] [ text <| "Started " ++ toClock elapsed ++ " ago." ]
                                        , div [] [ text <| toClock remaining ++ " remaining." ]
                                        , button [ onClick CancelJob ] [ text "Cancel Job" ]
                                        ]

                                    else
                                        [ div [] [ text <| "Stopping job for " ++ toClock job.duration ++ "." ]
                                        , paddedButton [ onClick KillJob ] [ text "Kill Job" ]
                                        ]
            )
        , formSection "General"
            [ textField "Port Name" prt.name UpdateName
            , textArea "Notes" prt.notes UpdateNotes
            ]
        , formSection "Connection"
            [ portNetworkParams prt.connection |> Html.map UpdateConnection
            ]
        , formSection "Danger Zone"
            [ button [ onClick DeletePort ] [ text "Delete Port" ]
            ]
        ]


formSection : String -> List (Html msg) -> Html msg
formSection title children =
    div []
        [ h4 []
            [ text title
            ]
        , div [] children
        ]


paddedButton : List (Attribute msg) -> List (Html msg) -> Html msg
paddedButton =
    styled button [ marginTop (C.rem 1) ]


portContainer : List (Attribute msg) -> List (Html msg) -> Html msg
portContainer =
    styled div
        [ displayFlex
        , flexDirection column
        , alignItems flexStart
        ]


textField : String -> String -> (String -> msg) -> Html msg
textField name v msg =
    let
        inner v =
            toUnstyled <|
                label
                    [ css
                        [ displayFlex
                        , flexDirection column
                        , marginBottom (C.rem 0.5)
                        ]
                    ]
                    [ span [ css [ color (rgba 0 0 0 0.8) ] ] [ text name ]
                    , input [ type_ "text", value v, onInput msg ] []
                    ]
    in
    lazy inner v


radioField : String -> List ( String, msg ) -> String -> Html msg
radioField name options v =
    let
        buttonFromOption : ( String, msg ) -> Html msg
        buttonFromOption ( lab, ev ) =
            label [ onClick ev, css [ marginRight (C.em 1) ] ]
                [ input
                    [ type_ "radio"
                    , A.checked (lab == v)
                    , css [ marginLeft (C.em 0.5) ]
                    ]
                    []
                , text lab
                ]
    in
    div
        [ css
            [ displayFlex
            , flexDirection column
            , marginBottom (C.rem 0.5)
            ]
        ]
        [ span [ css [ color (rgba 0 0 0 0.8) ] ] [ text name ]
        , div [ css [ displayFlex ] ] <| List.map buttonFromOption options
        ]


intField : String -> Int -> (Int -> msg) -> Html msg
intField name v msg =
    let
        handleInput newValue =
            msg <|
                if newValue == "" then
                    0

                else
                    String.toInt newValue
                        |> Result.toMaybe
                        |> Maybe.withDefault v
    in
    label
        [ css
            [ displayFlex
            , flexDirection column
            , marginBottom (C.rem 0.5)
            ]
        ]
        [ span [ css [ color (rgba 0 0 0 0.8) ] ] [ text name ]
        , input [ type_ "number", value (toString v), onInput handleInput ] []
        ]


incDecFloatField : String -> Float -> (Float -> msg) -> Html msg
incDecFloatField name v msg =
    let
        inner v =
            toUnstyled <|
                div
                    [ css
                        [ displayFlex
                        , alignItems flexEnd
                        , marginBottom (C.rem 0.5)
                        ]
                    ]
                    [ floatField name v msg
                    , button
                        [ onClick dec
                        , css [ marginLeft (C.rem 1) ]
                        ]
                        [ text "-" ]
                    , button
                        [ onClick inc
                        , css [ marginLeft (C.rem 0.5) ]
                        ]
                        [ text "+" ]
                    ]

        inc =
            msg <| v + 1

        dec =
            msg <| v - 1
    in
    lazy inner v


floatField : String -> Float -> (Float -> msg) -> Html msg
floatField name v msg =
    let
        handleInput newValue =
            msg <|
                if newValue == "" then
                    0

                else
                    String.toFloat newValue
                        |> Result.toMaybe
                        |> Maybe.withDefault v
    in
    label
        [ css
            [ displayFlex
            , flexDirection column
            , marginBottom (C.rem 0.5)
            ]
        ]
        [ span [ css [ color (rgba 0 0 0 0.8) ] ] [ text name ]
        , input [ type_ "number", value (toString v), onInput handleInput ] []
        ]


textArea : String -> String -> (String -> msg) -> Html msg
textArea name v msg =
    let
        inner v =
            toUnstyled <|
                label
                    [ css
                        [ displayFlex
                        , flexDirection column
                        , marginBottom (C.rem 0.5)
                        ]
                    ]
                    [ span [ css [ color (rgba 0 0 0 0.8) ] ] [ text name ]
                    , textarea
                        [ value v
                        , onInput msg
                        , css [ border3 (px 1) solid (hex "#ccc") ]
                        ]
                        []
                    ]
    in
    lazy inner v


portNetworkParams : PortConnection -> Html ConnectionMsg
portNetworkParams conn =
    div []
        ([--     div []
          -- [ radioField "Connection Type"
          --     [ ( "Legacy", UseLegacyConnection )
          --     -- ,
          --     -- ( "LoRaWan", UseLoRaWanConnection )
          --     ]
          --     (case conn of
          --         V1Connection _ ->
          --             "Legacy"
          --         -- LoRaWanConnection _ ->
          --         --     "LoRaWan"
          --     )
          -- ]
         ]
            ++ (case conn of
                    V1Connection { deviceId, portNumber } ->
                        [ textField "Node ID" deviceId UpdateDeviceId
                        , textField "Port Number" portNumber UpdatePortNumber
                        ]
                -- LoRaWanConnection { deviceId, portNumber } ->
                --     [ textField "Device ID" deviceId UpdateDeviceId
                --     , textField "Port Number" portNumber UpdatePortNumber
                --     ]
               )
        )



---- Model ----


type alias Model =
    { minutes : Float
    , seconds : Float
    , hours : Float
    }


type Msg
    = UpdateName String
    | UpdateConnection ConnectionMsg
    | UpdateNotes String
    | UpdateHours Float
    | UpdateMinutes Float
    | UpdateSeconds Float
    | CreateJob
    | KillJob
    | DeletePort
    | CancelJob


type ConnectionMsg
    = UpdateDeviceId String
    | UpdatePortNumber String
    | UseLegacyConnection



-- | UseLoRaWanConnection


initialModel : Model
initialModel =
    Model 0 0 0



---- Update ----


update : Msg -> Model -> IoPort -> ( Model, IoPort )
update msg model prt =
    case msg of
        UpdateName newName ->
            ( model, { prt | name = newName } )

        UpdateConnection msg ->
            ( model, { prt | connection = updateConnection msg prt.connection } )

        UpdateNotes newValue ->
            ( model, { prt | notes = newValue } )

        UpdateHours newValue ->
            ( if newValue >= 0 then
                { model | hours = newValue }

              else
                model
            , prt
            )

        UpdateMinutes newValue ->
            ( if newValue >= 0 then
                { model | minutes = newValue }

              else
                model
            , prt
            )

        UpdateSeconds newValue ->
            ( if newValue >= 0 then
                { model | seconds = newValue }

              else
                model
            , prt
            )

        CreateJob ->
            let
                newPrt =
                    if duration > 0 then
                        { prt
                            | job =
                                Just <|
                                    createJob <|
                                        toTime model
                        }

                    else
                        prt

                duration =
                    toTime model
            in
            ( model
            , newPrt
            )

        KillJob ->
            ( model, { prt | job = Nothing } )

        DeletePort ->
            -- handeled in main updater
            ( model, prt )

        CancelJob ->
            let
                newJob =
                    case prt.job of
                        Nothing ->
                            Nothing

                        Just job ->
                            Just { job | canceled = True }
            in
            ( model, { prt | job = newJob } )


updateConnection : ConnectionMsg -> PortConnection -> PortConnection
updateConnection msg connection =
    case connection of
        V1Connection cx ->
            case msg of
                UpdateDeviceId newId ->
                    V1Connection { cx | deviceId = newId }

                UpdatePortNumber newPort ->
                    V1Connection { cx | portNumber = newPort }

                UseLegacyConnection ->
                    V1Connection cx



-- UseLoRaWanConnection ->
--     LoRaWanConnection cx
-- LoRaWanConnection cx ->
--     case msg of
--         UpdateDeviceId newId ->
--             LoRaWanConnection { cx | deviceId = newId }
--         UpdatePortNumber newPort ->
--             LoRaWanConnection { cx | portNumber = newPort }
--         UseLegacyConnection ->
--             V1Connection cx
--         UseLoRaWanConnection ->
--             LoRaWanConnection cx
---- Util ----


toTime : { a | hours : Float, minutes : Float, seconds : Float } -> Time
toTime hms =
    0
        + (hms.hours * hour)
        + (hms.minutes * minute)
        + (hms.seconds * second)


toClock : Time -> String
toClock ms =
    let
        ( hours, minutes, seconds ) =
            toTimeComponents ms
    in
    [ hours, minutes, seconds ]
        |> List.map B.round
        |> List.map toString
        |> String.join ":"


toTimeComponents : Time -> ( Float, Float, Float )
toTimeComponents ms =
    let
        ( hours, notHours ) =
            divRest ms hour

        ( minutes, notMinutes ) =
            divRest notHours minute

        seconds =
            notMinutes / second
    in
    ( hours, minutes, seconds )


divRest : Float -> Float -> ( Float, Float )
divRest numerator denominator =
    let
        intDivResult =
            toFloat <| floor (numerator / denominator)

        rest =
            numerator - (intDivResult * denominator)
    in
    ( intDivResult, rest )
