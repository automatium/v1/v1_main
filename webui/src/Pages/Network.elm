module Pages.Network exposing (..)

import Data.Models exposing (PortsDict)
import Data.Route exposing (portPath)
import Data.Shared exposing (GUID)
import Data.Port as Port exposing (IoPort)
import Dict exposing (Dict, insert)
import Css as C exposing (..)
import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (class, style, css, href)
import Html.Styled.Keyed as Keyed
import Html.Styled.Events exposing (onClick)
import Msgs exposing (Msg(..))


view : Dict GUID IoPort -> Html Msg
view ports =
    networkWrapper []
        [ addPortButton, portList ports ]


networkWrapper : List (Attribute msg) -> List (Html msg) -> Html msg
networkWrapper =
    styled div
        [ displayFlex
        , flexDirection column
        , alignItems flexStart
        , flexGrow (int 1)
        , width (pct 100)
        , boxSizing borderBox
        ]


addPortButton : Html Msg
addPortButton =
    button [ onClick Msgs.AddPort ] [ text "Add Port" ]


portList : PortsDict -> Html msg
portList ports =
    Keyed.node "div" [ css [ maxWidth (pct 100) ] ] <| List.map keyedPort (Dict.toList ports)


keyedPort : ( GUID, IoPort ) -> ( GUID, Html msg )
keyedPort ( guid, prt ) =
    ( guid, viewPort prt )


viewPort : IoPort -> Html msg
viewPort prt =
    portContainer [ href <| portPath prt.guid ]
        [ portValue
            [ css [ flex (num 1) ]
            ]
            [ text <| portStatusText prt.job ]
        , portName
            [ css [ flex (num 1), displayFlex, justifyContent center ]
            ]
            [ text prt.name ]
        , portEditLink
            [ css [ flex (num 1), displayFlex, justifyContent flexEnd ]
            , href <| portPath prt.guid
            ]
            [ text "details" ]
        ]


portContainer : List (Attribute msg) -> List (Html msg) -> Html msg
portContainer =
    styled a
        [ border3 (px 1) solid (rgba 0 0 0 0.2)
        , borderRadius (px 5)
        , margin (px 10)
        , padding2 zero (px 10)
        , displayFlex
        , flexDirection row
        , alignItems center
        , justifyContent spaceBetween
        , backgroundColor (rgba 255 255 255 1)
        , height (px 35)
        , width (px 500)
        , maxWidth (pct 100)
        , boxSizing borderBox
        , color inherit
        , textDecoration inherit
        ]


portName : List (Attribute msg) -> List (Html msg) -> Html msg
portName =
    styled span
        [ fontWeight (int 600)
        ]


portValue : List (Attribute msg) -> List (Html msg) -> Html msg
portValue =
    styled span []


portStatusText : Maybe Port.Job -> String
portStatusText maybeJob =
    case maybeJob of
        Nothing ->
            "Off"

        Just job ->
            case job.started of
                Nothing ->
                    "Queued"

                Just started ->
                    "Running"


portEditLink : List (Attribute msg) -> List (Html msg) -> Html msg
portEditLink =
    styled a []
