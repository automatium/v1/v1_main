module Msgs exposing (..)

import Navigation exposing (Location)
import Pages.Port as Port
import Http
import Data.Port exposing (IoPort)
import Data.Shared exposing (GUID)
import Time exposing (Time)


type Msg
    = NoOp
    | OnLocationChange Location
    | AddPort
    | UpdatePort Port.Msg
    | SavedPort (Revert GUID IoPort) (Result Http.Error IoPort)
    | GetPorts
    | GotPorts (Result Http.Error { time : Time, ports : List IoPort })
    | Tick Time
    | DeletedPort IoPort (Result Http.Error String)


type Revert key value
    = DeleteRevert key
    | RevertTo value
