import 'typeface-lato'
import 'typeface-source-sans-pro'
import './normalize.css'
import './main.css'
import { Main } from './Main.elm'
import registerServiceWorker from './registerServiceWorker'

// document.body.addEventListener(
//   'touchend',
//   function(e) {
//     e.preventDefault()
//     console.log(e.target)
//     e.target.click()
//   },
//   true,
// )

Main.embed(document.getElementById('root'), {
  randomSeed: Math.floor(Math.random() * 0x0fffffff),
})

registerServiceWorker()
