module Request.Port exposing (GetPortsResult, connectionDecoder, date, deletePort, encodeConnection, encodeJob, encodePort, getPorts, getPortsDecoder, jobDecoder, portDecoder, portsDecoder, rf, ri, rs, savePort)

import Data.Port as Port exposing (IoPort, PortConnection(..), blankV1Connection)
import Data.Shared exposing (GUID)
import Date exposing (Date)
import Http
import Json.Decode exposing (..)
import Json.Decode.Pipeline exposing (decode, hardcoded, optional, required)
import Json.Encode as Encode
import Time exposing (Time)



-- Request Methods


getPorts : (Result Http.Error GetPortsResult -> msg) -> Cmd msg
getPorts msg =
    let
        url =
            "/ports/"

        request =
            Http.get url getPortsDecoder
    in
    Http.send msg request


savePort : (Result Http.Error IoPort -> msg) -> IoPort -> Cmd msg
savePort msg prt =
    let
        url =
            "ports/" ++ prt.guid ++ "/update"

        body =
            encodePort prt

        request =
            Http.post url (Http.jsonBody body) portDecoder
    in
    Http.send msg request


deletePort : (Result Http.Error String -> msg) -> GUID -> Cmd msg
deletePort msg guid =
    let
        url =
            "ports/" ++ guid ++ "/delete"

        request =
            Http.getString url
    in
    Http.send msg request



-- Decoders


type alias GetPortsResult =
    { time : Time
    , ports : List IoPort
    }


getPortsDecoder : Decoder GetPortsResult
getPortsDecoder =
    decode GetPortsResult
        |> rf "time"
        |> required "ports" portsDecoder


portsDecoder : Decoder (List IoPort)
portsDecoder =
    list portDecoder


portDecoder : Decoder IoPort
portDecoder =
    decode IoPort
        |> rs "guid"
        |> rs "name"
        |> required "connection" connectionDecoder
        |> required "job" (maybe jobDecoder)
        |> ri "version"
        |> rs "notes"


connectionDecoder : Decoder PortConnection
connectionDecoder =
    oneOf
        [ field "V1"
            (decode (\d p -> V1Connection { deviceId = d, portNumber = p })
                |> rs "device_id"
                |> rs "port_number"
            )

        -- , field "LoRaWan"
        --     (decode (\d p -> LoRaWanConnection { deviceId = d, portNumber = p })
        --         |> rs "device_id"
        --         |> rs "port_number"
        --     )
        ]


jobDecoder : Decoder Port.Job
jobDecoder =
    decode Port.Job
        |> required "duration" float
        |> required "started" (nullable float)
        |> required "canceled" bool


rs : String -> Decoder (String -> b) -> Decoder b
rs key =
    required key string


ri : String -> Decoder (Int -> b) -> Decoder b
ri key =
    required key int


rf : String -> Decoder (Float -> b) -> Decoder b
rf key =
    required key float


date : String -> Decoder Date
date dateString =
    case Date.fromString dateString of
        Ok date ->
            succeed date

        Err e ->
            fail e



-- Encoders


encodePort : IoPort -> Encode.Value
encodePort prt =
    Encode.object
        [ ( "guid", Encode.string prt.guid )
        , ( "name", Encode.string prt.name )
        , ( "connection", encodeConnection prt.connection )
        , ( "job", encodeJob prt.job )
        , ( "version", Encode.int prt.version )
        , ( "notes", Encode.string prt.notes )
        ]


encodeConnection : PortConnection -> Encode.Value
encodeConnection portConnection =
    case portConnection of
        V1Connection con ->
            Encode.object
                [ ( "V1"
                  , Encode.object
                        [ ( "device_id", Encode.string con.deviceId )
                        , ( "port_number", Encode.string con.portNumber )
                        ]
                  )
                ]



-- LoRaWanConnection con ->
--     Encode.object
--         [ ( "LoRaWan"
--           , Encode.object
--                 [ ( "device_id", Encode.string con.deviceId )
--                 , ( "port_number", Encode.string con.portNumber )
--                 ]
--           )
--         ]


encodeJob : Maybe Port.Job -> Encode.Value
encodeJob maybeJob =
    case maybeJob of
        Nothing ->
            Encode.null

        Just job ->
            Encode.object
                [ ( "duration", Encode.int <| round job.duration )
                , ( "canceled", Encode.bool job.canceled )
                , ( "started"
                  , case job.started of
                        Just started ->
                            Encode.string <| toString started

                        Nothing ->
                            Encode.null
                  )
                ]
