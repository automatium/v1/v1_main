module Update exposing (..)

import Data.Models exposing (Model)
import Data.Port exposing (IoTask(..), IoPort, makePort, blankV1Connection, PortType(..), PortConnection(..), IoTask)
import Data.Route exposing (Route(..), parseLocation, networkPath, portPath)
import Data.Shared exposing (GUID)
import Pages.Port as Port
import Msgs exposing (..)
import Uuid.Barebones as Uuid
import Random.Pcg exposing (step)
import Dict exposing (Dict)
import Request.Port exposing (getPorts, savePort, deletePort)
import Navigation exposing (newUrl)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    updateWithRoute (model.route) msg model


updateWithRoute : Route -> Msg -> Model -> ( Model, Cmd Msg )
updateWithRoute route msg model =
    let
        delegateDictUpdate updater msg maybeEntry =
            case maybeEntry of
                Nothing ->
                    Nothing

                Just entry ->
                    Just (updater msg entry)
    in
        case ( msg, route ) of
            ( OnLocationChange location, _ ) ->
                let
                    newRoute =
                        parseLocation location
                in
                    ( { model | route = newRoute }, Cmd.none )

            ( NoOp, _ ) ->
                ( model, Cmd.none )

            ( AddPort, _ ) ->
                let
                    ( newUuid, newSeed ) =
                        step Uuid.uuidStringGenerator model.uuidSeed

                    newPort =
                        makePort newUuid "Untitled Port" blankV1Connection Nothing
                in
                    ( { model
                        | ports = (Dict.insert newUuid newPort model.ports)
                        , uuidSeed = newSeed
                      }
                    , Cmd.none
                    )

            ( UpdatePort Port.DeletePort, PortRoute guid ) ->
                case Dict.get guid model.ports of
                    Nothing ->
                        ( model, Cmd.none )

                    Just existingPort ->
                        let
                            ( newPortPage, _ ) =
                                Port.update Port.DeletePort model.portPage existingPort
                        in
                            ( { model
                                | ports = Dict.remove guid model.ports
                                , portPage = newPortPage
                              }
                            , Cmd.batch
                                [ newUrl <| networkPath
                                , deletePort (DeletedPort existingPort) guid
                                ]
                            )

            ( UpdatePort msg, PortRoute guid ) ->
                case Dict.get guid model.ports of
                    Nothing ->
                        ( model, Cmd.none )

                    Just existingPort ->
                        let
                            ( newPortPage, updatedPort ) =
                                Port.update msg model.portPage existingPort

                            newPort =
                                { updatedPort | version = updatedPort.version + 1 }
                        in
                            ( { model
                                | ports = Dict.insert guid newPort model.ports
                                , portPage = newPortPage
                              }
                            , savePort (SavedPort (RevertTo existingPort)) newPort
                            )

            ( SavedPort _ (Ok newItem), _ ) ->
                let
                    newModel =
                        case Dict.get newItem.guid model.ports of
                            Nothing ->
                                model

                            Just existingItem ->
                                case newItem.version >= existingItem.version of
                                    False ->
                                        model

                                    True ->
                                        { model | ports = Dict.insert newItem.guid newItem model.ports }
                in
                    ( newModel
                    , Cmd.none
                    )

            ( SavedPort revert (Err _), _ ) ->
                -- revert on failure
                ( case revert of
                    RevertTo oldItem ->
                        -- revert to the old version to stay consistent
                        let
                            updateItem maybeItem =
                                case maybeItem of
                                    Nothing ->
                                        Nothing

                                    Just currentItem ->
                                        Just oldItem
                        in
                            { model | ports = Dict.insert oldItem.guid oldItem model.ports }

                    DeleteRevert guid ->
                        -- this item was never saved before, so reverting means deleting
                        { model | ports = Dict.remove guid model.ports }
                , Cmd.none
                )

            -- ( UpdatePort portMsg, PortRoute guid ) ->
            --     ( { model | ports = Dict.update guid (delegateDictUpdate Port.update portMsg) model.ports }, update )
            -- ( GetPorts, _ ) ->
            --     ( model, getPorts GotPorts )
            ( GotPorts (Ok new), _ ) ->
                let
                    newPorts =
                        List.foldl insertPort model.ports new.ports

                    insertPort : IoPort -> Dict GUID IoPort -> Dict GUID IoPort
                    insertPort prt ports =
                        Dict.update prt.guid (insertIfNewer prt) ports

                    insertIfNewer newPort maybeExistingPort =
                        case maybeExistingPort of
                            Nothing ->
                                Just newPort

                            Just existingPort ->
                                if existingPort.version < newPort.version then
                                    Just newPort
                                else
                                    Just existingPort
                in
                    ( { model
                        | ports = newPorts
                        , time = new.time
                      }
                    , Cmd.none
                    )

            ( Tick _, _ ) ->
                ( model, getPorts GotPorts )

            ( DeletedPort revert (Err _), _ ) ->
                let
                    applyDelete maybeItem =
                        case maybeItem of
                            Nothing ->
                                Nothing

                            Just item ->
                                Just { item | pendingDeletion = False }
                in
                    ( { model
                        | ports = Dict.insert revert.guid revert model.ports
                      }
                    , newUrl <| portPath revert.guid
                    )

            ( _, _ ) ->
                ( model, Cmd.none )
