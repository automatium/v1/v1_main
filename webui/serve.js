const express = require('express')
const proxy = require('http-proxy-middleware')
const path = require('path')

var app = express()

app.use(express.static(path.join(__dirname, 'build')))
app.use(proxy({ target: 'http://localhost:7878', changeOrigin: true }))

const listenPort = process.env.PORT || 4500
app.listen(listenPort, (err) => {
  if (err) {
    console.log(err)
    process.exit(1)
  }

  console.log(`Listening on *:${listenPort}`)
})
