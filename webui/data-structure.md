# Data structure architecture

## For control algorithms

The base unit of control is a port. The control algorithms work almost exclusively at this level.

The scheduler will want to know:

* What is the value of this port
* What networks connect to this port?
* What ports are reachable from this one (via inter-module, i2c, or wireless connections)
* What control-algorithm does this port obey?

## For the UI

The user will want to know:

**Where is the port controlling overhead irrigation at greenhouse 2?**
Okay, so we need a location attribute in addition to the name. It would be named "overhead" and would have "greenhouse 2" set as the location.

**Show me all my coolers.**
It's getting hot and I want to make sure none of them are overheating.

Hmmm, looks like we need a new "role" attribute. But role in what sense? Role as in temperature sensor, or role as in arbitrary grouping.

Probably the most elegant way to solve this would be custom groups. Want to see certain things together? Make a group for them.

**I'm making a CoolBot clone. I want to zero in on everything connected to that particular Wireless Brain.**

Clearly we need a way to view everything on a specific wired network. The ports shouldn't be floating around.

**I'm wiring up a set of valve outputs**

Need a way to zero in on a specific module, too.

**Show me all my temperature sensors**

In other words, filter by quantity being measured. Pretty easy.

**I want to organize my fields by X custom value.**
No problem! We allow you to do that.

**I'm not a programmer. How do I run these filters?**
It has to be useable by normal people. Close.io is really great on this front!

A big question for me is whether the local network/module they're connected to should be a smart view, or a dedicated interface?

A hierarchical system with a dedicated interface sounds intuitive, but what would you name each wired grouping of devices?

* For a field manager with outputs for blocks 1, 2, 3, and 4. "Block 1-4 manager"
* For a network in a greenhouse. "Greenhouse 2 entry"

Hmm, this would be drastically simplified if the location was set at the module chain level.

Really the problem here is what level do you set the location? You can have high-level locations like "Field 1", lower level locations like "Greenhouse 5 entry", and even aggregate locations like "Block 1, 2, 3, 4". If I were programming, I would create a syntax like "field1::{block1, block2, block3, block4}". Theoretically I could make a nice UI for this.

But why do I actually need this?

Assuming we don't have a dedicated "smart view", how would you find a valve? Assuming it's set up, you could do something like this:

* Select another valve on the same chain.

**I want to set up a new sprinkler**
