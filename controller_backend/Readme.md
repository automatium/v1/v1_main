# Au System Controller

A new controller backend written in Rust. The previous controller was the OpenSprinkler firmware sending HTTP requests to a Node.js server.

## Networking

I'm going to start with a simple REST API, using the Warp library.

`GET /network_state` returns network state.

`POST /ports/:guid` updates basic port params (currently name)

`PUT /ports/:guid/task` replaces the tasks with a new one

## Task Executor

Goal: be resilient to all types of failures.  Rebooting the Pi shouldn't be a problem.
Types of tasks: run for x minutes.

If the port fails to turn on. Report error and keep trying until it succeeds.

## Persistence

I also need a database backend. Currently I'm using Redis becuase Rust has a robust Redis client.  I would like to switch to RocksDB or Sled in future.  (see #1)

Right now I only need to store a few things:

* Port definitions
* In-flight tasks
