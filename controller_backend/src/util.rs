use std::time::{SystemTime, UNIX_EPOCH};

pub fn now() -> u64 {
    match SystemTime::now().duration_since(UNIX_EPOCH) {
        Ok(n) => n.as_secs() * 1000 + u64::from(n.subsec_millis()),
        Err(_) => panic!("SystemTime before UNIX EPOCH!"),
    }
}
