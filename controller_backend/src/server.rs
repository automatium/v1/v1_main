mod port;

use crate::SharedData;
use failure::Error;
use warp::{http::StatusCode, path, Filter, Rejection, Reply};

pub fn run(_data: SharedData) -> Result<(), Error> {

    // These are some `Filter`s that several of the endpoints share,
    // so we'll define them here and reuse them below...

    // Turn our "state", our db, into a Filter so we can combine it
    // easily with others...
    // let with_data = warp::any().map(move || shared_data.clone());

    // home and hello
    let root = warp::get2()
        .and(warp::path::end())
        .map(|| String::from("Sprinkler API"));
    let hello = path!("hello" / String).map(|name| format!("Hello, {}!", name));
    let favicon = path!("favicon.ico").map(|| String::new());
    let misc_routes = root.or(hello).or(favicon);

    // ports
    let all_ports = warp::get2().and(path!("ports")).and_then(port::all);
    let update_port = warp::post2()
        .and(path!("ports" / String / "update"))
        .and(warp::body::json())
        .and_then(port::update);
    let delete_port = path!("ports" / String / "delete").and_then(port::delete);
    let ports_api = all_ports.or(update_port).or(delete_port);

    // compose routes, add logging middleware
    let routes = misc_routes
        .or(ports_api)
        .with(warp::log("warp"))
        .recover(mask_error);

    // start the server!
    warp::serve(routes).run(([0, 0, 0, 0], 7000));

    Ok(())
}

fn mask_error(err: Rejection) -> Result<impl Reply, Rejection> {
    match err.cause() {
        Some(c) => {
            println!("Internal server error: {:?}", c);
            Ok(StatusCode::INTERNAL_SERVER_ERROR)
        }
        None => Err(err),
    }
}
