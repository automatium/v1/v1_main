use redis;
use reqwest;

use self::redis::{Commands, Iter};
use crate::db::redis::{db_key, get_connection};
use crate::defs::V1_VALVE_SERVER;
use crate::lora::LoRa;
use crate::model::port::Connection;
use crate::model::port::{Job, Port};
use crate::util::now;
use crate::SharedData;
use failure::{err_msg, Error, ResultExt};
use std::{thread, time};

pub fn run(_data: SharedData) -> Result<(), Error> {
    let db = get_connection();
    let mut lora = LoRa::from_auto()?;

    loop {
        if let Err(e) = do_loop(&db, &mut lora) {
            println!("Error in scheduler!\n{:?}", e)
        }
    }
}

fn do_loop(db: &redis::Connection, lora: &mut LoRa) -> Result<(), Error> {
    let keys: Iter<String> = db
        .scan_match(db_key("ports:*"))
        .context("Error scanning ports.")?;

    for key in keys {
        // watch the key
        redis::cmd("WATCH")
            .arg(&key)
            .query::<()>(db)
            .context("Error watching port.")?;

        let port_string: String = db.get(&key).context("Error reading port.")?;
        let port = Port::from_str(&port_string)?;

        if let Some(job) = port.job {
            let job_update: Option<Option<Job>> = match job.started {
                // job is already started
                Some(started) => {
                    if job.canceled || times_up(job.duration, started) {
                        // stop the job and remove it from the DB
                        println!("Stopping port {}.  Current time: {}", key, now());
                        match port.connection {
                            Connection::V1 {
                                ref device_id,
                                ref port_number,
                            } => {
                                // let url = String::from(V1_VALVE_SERVER)
                                //     + "/send/"
                                //     + device_id
                                //     + "/E"
                                //     + port_number;
                                // println!("GET {}", url);
                                // let response =
                                //     reqwest::get(&url).context("Error contacting valve server.")?;
                                // if !response.status().is_success() {
                                //     return Err(err_msg("Couldn't send command to device"));
                                // }

                                lora.send_wait(2, "l0".into())
                                    .context("...while sending command to close the valve")?;

                                println!("Stopped port {}.  Current time: {}", key, now());
                            }
                        };
                        Some(None)
                    } else {
                        // let it run!
                        None
                    }
                }
                None => {
                    if job.canceled {
                        // The job was canceled before we got a chance to start it, so we can safely delete it.
                        // NOTE: This relies on the updater pausing until we know whether the port has successfully started
                        // FIXME: Refactor the job definition to include a "starting" state that open the way for an async scheduler
                        println!(
                            "Port {} was canceled before it was started. Deleting the job.",
                            key
                        );
                        Some(None)
                    } else {
                        // start the job!
                        println!("Starting port {}", key);
                        match port.connection {
                            Connection::V1 {
                                ref device_id,
                                ref port_number,
                            } => {
                                // let url = String::from(V1_VALVE_SERVER)
                                //     + "/send/"
                                //     + device_id
                                //     + "/S"
                                //     + &port_number;
                                // println!("GET {}", url);
                                // let response =
                                //     reqwest::get(&url).context("Error contacting valve server.")?;
                                // if !response.status().is_success() {
                                //     return Err(err_msg("Couldn't send command to device"));
                                // }

                                lora.send_wait(2, "l1".into())
                                    .context("...while sending command to open the valve")?;

                                println!("Started port {}.  Current time: {}", key, now());
                            }
                        }
                        let start_time = now();
                        println!("Started job at {}", start_time);
                        Some(Some(Job {
                            started: Some(start_time),
                            ..job
                        }))
                    }
                }
            };

            if let Some(new_job) = job_update {
                let new_port = Port {
                    version: port.version + 1,
                    job: new_job,
                    ..port
                };

                let port_db_string = new_port.to_db_string()?;

                redis::pipe()
                    .atomic()
                    .cmd("SET")
                    .arg(&key)
                    .arg(&port_db_string)
                    .query::<()>(db)?;
            }
        }

        // unwatch the db key, just to be safe!
        redis::cmd("UNWATCH").query::<()>(db)?;
    }

    thread::sleep(time::Duration::from_millis(1000));

    Ok(())
}

fn times_up(duration: u64, started: u64) -> bool {
    let now = now();

    now >= started + duration
}
