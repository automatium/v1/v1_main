use crate::defs::FETCH_TIMEOUT_MS;
use crate::proto::{parse_status, Status};
use anyhow::{anyhow, Context, Error};
use log::{info, log_enabled, trace, Level::*};
use serialport::{prelude::*, SerialPortSettings, SerialPortType::*};
use std::io;
use std::time::{Duration as StdDuration, Instant};

#[allow(dead_code)]
pub enum SerialRequest {
    Status,
    SwitchPort { port: u8, on: bool },
}
use SerialRequest::*;

pub fn fetch_and_parse(
    port: &mut Box<dyn SerialPort>,
    request: &SerialRequest,
) -> Result<Status, Error> {
    let msg = match request {
        Status => format!("<S>"),
        SwitchPort { on, port } => {
            let state = if *on { 1 } else { 0 };
            format!("<V{}{}>", port, state)
        }
    };

    let result = serial_fetch(port, &msg).context("Serial fetch")?;
    let result = parse_status(&result).context("parsing serial response")?;

    Ok(result)
}

pub fn serial_fetch(port: &mut Box<dyn SerialPort>, msg: &str) -> Result<String, Error> {
    trace!("serial fetch");

    // log our message
    // we print directly to avoid the date/time headers
    if log_enabled!(Debug) {
        println!("-> {}", msg);
    }

    // write the msg to the port
    port.write_all(msg.as_bytes())
        .context("Error writing message to port")?;

    // write a newline (ASCII 10)
    port.write_all(b"\n")
        .context("Error writing newline to port")?;

    // wait for a response
    let mut response_buffer: String = String::new();
    let start_time = Instant::now();

    let mut printed_leading_arrow = false;
    let mut inside_response = false;
    loop {
        if start_time.elapsed() > StdDuration::from_millis(FETCH_TIMEOUT_MS) {
            return Err(anyhow!("Timed out waiting for response from modem."));
        }

        let mut char_buf = [0; 1];
        match port.read(&mut char_buf) {
            // no bytes available at this time
            Ok(bytes) if bytes == 0 => (),
            // another way of signaling no bytes available at this time
            Err(ref e) if e.kind() == io::ErrorKind::TimedOut => (),
            // we got a byte!
            Ok(_bytes) => {
                // the byte
                let chr = std::char::from_u32(char_buf[0] as u32)
                    .ok_or_else(|| anyhow!("text is not valid ascii"))?;

                // log the messages
                if log_enabled!(Debug) {
                    if !printed_leading_arrow {
                        print!("<- ");
                        printed_leading_arrow = true;
                    }
                    for c in chr.escape_debug() {
                        print!("{}", c);
                    }
                }

                if inside_response {
                    // check for end character
                    if chr == '>' {
                        if log_enabled!(Debug) {
                            println!("");
                        }
                        return Ok(response_buffer);
                    } else {
                        response_buffer.push(chr);
                    }
                } else {
                    if chr == '<' {
                        inside_response = true
                    }
                }
            }
            // real error
            Err(e) => {
                return Err(e).context("reading from serial port");
            }
        }
    }
}

pub fn get_port() -> Result<Box<dyn SerialPort>, Error> {
    let port_name = match std::env::var("LORA_SERIAL_PORT") {
        Ok(val) => Ok(val),
        Err(_e) => {
            info!("No serial port was specified, using the first USB serial port.");
            serialport::available_ports()
                .context("Getting available ports")?
                .into_iter()
                .find_map(|port_info| match &port_info.port_type {
                    UsbPort(_usb_info) => Some(port_info.port_name),
                    _ => None,
                })
                .ok_or(anyhow!("No port was found"))
        }
    }?;

    let settings = SerialPortSettings {
        baud_rate: 115200,
        data_bits: DataBits::Eight,
        flow_control: FlowControl::None,
        parity: Parity::None,
        stop_bits: StopBits::One,
        timeout: StdDuration::from_millis(1),
    };

    Ok(serialport::open_with_settings(&port_name, &settings)
        .context("opening port with settings")?)
}
