extern crate redis;
use lazy_static::lazy_static;

use crate::defs::{DB_PREFIX, REDIS_URL};

pub fn db_key(key: &str) -> String {
    DB_PREFIX.to_owned() + key
}

pub fn get_connection() -> redis::Connection {
    lazy_static! {
        static ref CLIENT: redis::Client = redis::Client::open(REDIS_URL).unwrap();
    }
    CLIENT.get_connection().unwrap()
}