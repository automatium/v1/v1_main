use super::shared::GUID;
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Debug)]
pub struct NetworkState {
    pub ports: HashMap<GUID, Port>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Port {
    pub guid: GUID,
    pub name: String,
    pub connection: Connection,
    pub task: Task,
}

#[derive(Serialize, Deserialize, Debug)]
pub enum Connection {
    V1Connection {
        device_id: String,
        port_number: String,
    },
}

#[derive(Serialize, Deserialize, Debug)]
pub enum Task {
    NoTask,
    ScheduledTask { runtime: u64 },
    RunningTask { runtime: u64, started_at: u64 },
    ErroredTask { runtime: u64, error: String },
}
