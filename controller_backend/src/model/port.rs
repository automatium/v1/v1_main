use ron;
use serde::{Deserialize, Serialize};
use serde_json;

use super::shared::GUID;

/// Alias to the latest item version
pub type Port = PortV3;

impl Port {
    /// Convert an `Item` to the latest serialization format
    /// Don't use when reading from the client
    pub fn to_db_string(&self) -> Result<String, serde_json::Error> {
        use self::VersionedPortRef::*;
        serde_json::to_string_pretty(&V3(self))
    }

    /// Deserialize an `Item` from any known format/version combination
    /// This is only for read/write from the db.  Use serde_json directly for client messages
    pub fn from_str(input: &str) -> Result<Port, serde_json::Error> {
        use self::VersionedPortOwned::*;

        // attempt to convert from json string
        // I'm using a match for the top level to capture the original error
        match serde_json::from_str::<VersionedPortOwned>(input) {
            Ok(poly_item) => Ok(match poly_item {
                V1(item) => item.into(),
                V2(item) => item.into(),
                V3(item) => item.into(),
            }),
            Err(parse_err) => {
                // If conversion fails, attempt to convert from other known formats

                // Attempt to convert from RON
                // We didn't have any concept of versions back then, so we convert to v1
                if let Ok(item) = ron::de::from_str::<PortV1>(input) {
                    return Ok(item.into());
                };

                // return the original parse error, to optimize for the most common case
                Err(parse_err)
            }
        }
    }
}

/// List of all item versions, by reference for serialization
#[derive(Debug, Serialize)]
enum VersionedPortRef<'a> {
    V3(&'a PortV3),
    // V2(&'a PortV2),
    // V1(&'a PortV1),
}

/// List of all item versions, owned for deserialization
#[derive(Debug, Deserialize)]
enum VersionedPortOwned {
    V3(PortV3),
    V2(PortV2),
    V1(PortV1),
}

#[derive(Serialize, Deserialize, Debug)]
pub struct PortV3 {
    pub guid: GUID,
    pub name: String,
    pub connection: ConnectionV2,
    pub job: Option<JobV1>,
    pub version: u64,
    pub notes: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct PortV2 {
    pub guid: GUID,
    pub name: String,
    pub connection: ConnectionV1,
    pub job: Option<JobV1>,
    pub version: u64,
    pub notes: String,
}

impl From<PortV2> for Port {
    fn from(o: PortV2) -> Self {
        Port {
            guid: o.guid,
            name: o.name,
            connection: o.connection.into(),
            job: o.job,
            version: o.version,
            notes: o.notes,
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct PortV1 {
    pub guid: GUID,
    pub name: String,
    pub connection: ConnectionV1,
    pub task: TaskV1,
}

impl From<PortV1> for Port {
    fn from(o: PortV1) -> Self {
        Port {
            guid: o.guid,
            name: o.name,
            connection: o.connection.into(),
            job: None,
            version: 0,
            notes: String::new(),
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub enum TaskV1 {
    NoTask,
    ScheduledTask { runtime: u64 },
    RunningTask { runtime: u64, started_at: u64 },
    ErroredTask { runtime: u64, error: String },
}

pub type Job = JobV1;

#[derive(Serialize, Deserialize, Debug)]
pub struct JobV1 {
    pub duration: u64,
    pub started: Option<u64>,
    #[serde(default)]
    pub canceled: bool,
}

impl Default for JobV1 {
    fn default() -> Self {
        JobV1 {
            duration: 0,
            started: None,
            canceled: false,
        }
    }
}

// Note the difference between serialization version and connection version
// Each serialization version is represented by a top-level enum
// Each enum contains the different connection versions
pub use self::ConnectionV2 as Connection;

#[derive(Serialize, Deserialize, Debug)]
pub enum ConnectionV2 {
    V1 {
        device_id: String,
        port_number: String,
    },
    // LoRaWan {
    //     device_id: String,
    //     port_number: String,
    // },
}

#[derive(Serialize, Deserialize, Debug)]
pub enum ConnectionV1 {
    V1Connection {
        device_id: String,
        port_number: String,
    },
}

impl From<ConnectionV1> for Connection {
    fn from(original: ConnectionV1) -> Self {
        match original {
            ConnectionV1::V1Connection {
                device_id,
                port_number,
            } => ConnectionV2::V1 {
                device_id,
                port_number,
            },
        }
    }
}
