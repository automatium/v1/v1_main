mod defs;
mod proto;
mod serial;

use anyhow::Error;
use serial::{fetch_and_parse, get_port, SerialRequest};

fn main() -> Result<(), Error> {
    env_logger::init();

    let mut serial = get_port()?;

    let status = fetch_and_parse(
        &mut serial,
        // &SerialRequest::SwitchPort { port: 1, on: true },
        &SerialRequest::Status,
    );
    println!("{:?}", status);

    Ok(())
}
