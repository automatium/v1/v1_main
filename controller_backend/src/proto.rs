use anyhow::{anyhow, Error};
use nom::{
    branch::alt, bytes::complete::tag, character::complete::alphanumeric1 as alphanumeric,
    combinator::map, parse_to, sequence::tuple, IResult,
};

#[derive(Clone, Debug)]
pub struct Status {
    pub valve_state: [bool; 2],
    pub battery_reading: f64,
}

pub fn parse_status(input: &str) -> Result<Status, Error> {
    match do_parse_status(input) {
        Ok((_input, result)) => Ok(result),
        Err(e) => Err(anyhow!("{:?}", e)),
    }
}

pub fn do_parse_status(input: &str) -> IResult<&str, Status> {
    let start = tag("s1|");
    let separator = tag("|");

    let (input, _) = start(input)?;
    let (input, valve_state) = parse_valve_state(input)?;
    let (input, _) = separator(input)?;
    let (input, battery_reading) = parse_battery_reading(input)?;

    let status = Status {
        valve_state,
        battery_reading,
    };

    Ok((input, status))
}

fn parse_valve_state(input: &str) -> IResult<&str, [bool; 2]> {
    let (input, (valve0, valve1)) =
        tuple((parse_single_valve_state, parse_single_valve_state))(input)?;

    let result = [valve0, valve1];

    Ok((input, result))
}

fn parse_single_valve_state(input: &str) -> IResult<&str, bool> {
    alt((map(tag("0"), |_| false), map(tag("1"), |_| true)))(input)
}

fn parse_battery_reading(input: &str) -> IResult<&str, f64> {
    let (input, reading) = alphanumeric(input)?;
    let (_, reading) = parse_to!(reading, u64)?;

    let reading = (reading as f64) * 2.0 * 0.003226;

    Ok((input, reading))
}
