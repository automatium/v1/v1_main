use self::redis::{Commands, Iter};
use crate::db::redis::{db_key, get_connection};
use crate::defs::REDIS_URL;
use crate::model::port::Port;
use crate::model::shared::GUID;
use crate::util::now;
use failure::{Error, ResultExt};
use redis;
use serde::Serialize;
use serde_json;
use warp::{Rejection, Reply};

macro_rules! expect {
    ($x:expr, $m:expr) => {
        match $x {
            Err(e) => {
                println!("{}\n{:?}", $m, e);
                panic!($m);
            }
            Ok(val) => val,
        }
    };
}

#[derive(Serialize, Debug)]
struct GetPortsResponse {
    ports: Vec<Port>,
    time: u64,
}

// TODO: make this async
pub fn all() -> Result<impl Reply, Rejection> {
    // connect to redis
    let client = redis::Client::open(REDIS_URL).unwrap();
    let con = client.get_connection().unwrap();

    let keys: Iter<String> = con.scan_match(db_key("ports:*")).unwrap();

    let ports: Vec<Port> = keys
        .map(|key| {
            let port_string: String = con.get(key).unwrap();
            expect!(
                Port::from_str(&port_string),
                format!("Error parsing item:\n{}", port_string)
            )
        })
        .collect();

    let time = now();

    let response = GetPortsResponse { time, ports };

    let response_string = expect!(serde_json::to_string(&response), "error serializing ports");

    Ok(response_string)
}

pub fn update(guid: GUID, new_item: Port) -> Result<impl Reply, Rejection> {
    // write to the DB, but don't overwrite a later version

    let con = get_connection();

    // move value into this closure
    let guid = guid;
    let dbkey = db_key("ports:") + &guid;

    // set a watch on the value in case another client modifies it

    redis::cmd("WATCH")
        .arg(&dbkey)
        .query::<()>(&con)
        .on_err(|| format!("Error watching {}", dbkey))?;

    // (attempt to) get the existing item
    // We won't see anything if this is a new item
    let existing_item_version = match con.get::<_, String>(&dbkey) {
        Ok(item_str) => {
            // let item = htry!(Item::from_str(&item_str), "Error parsing existing item");
            let item = Port::from_str(&item_str).unwrap();
            item.version
        }
        Err(_) => 0,
    };

    // set a new item, if needed
    if existing_item_version <= new_item.version {
        // create serialized RON for writing to db
        let port_db_string = Port::to_db_string(&new_item).on_err(|| "Error serializing item.")?;

        redis::pipe()
            .atomic()
            .cmd("SET")
            .arg(&dbkey)
            .arg(&port_db_string)
            .query::<()>(&con)
            .on_err(|| "error executing transaction")?;
    } else {
        redis::cmd("UNWATCH")
            .query::<()>(&con)
            .on_err(|| format!("Error unwatching {}", dbkey))?;
    }

    // create the new item to send to the client
    // let item_json =
    //     serde_json::to_string(&new_item).on_err(|| "Error serializing item for client")?;

    Ok(warp::reply::json(&new_item))
}

pub fn delete(guid: String) -> Result<impl Reply, Rejection> {
    // connect to Redis
    let con = get_connection();

    // write to the DB
    con.del::<_, ()>(db_key("ports:") + &guid).unwrap();

    Ok(String::new())
}

trait ServerResultExt<T, E> {
    fn on_err<M, S>(self, get_msg: M) -> Result<T, warp::reject::Rejection>
    where
        M: FnOnce() -> S,
        S: std::string::ToString;
}

impl<T, E: std::error::Error + Send + Sync + 'static> ServerResultExt<T, E> for Result<T, E> {
    fn on_err<M, S>(self, get_msg: M) -> Result<T, warp::reject::Rejection>
    where
        M: FnOnce() -> S,
        S: std::string::ToString,
    {
        // don't compute context unless it's an error
        if self.is_err() {
            let msg: String = get_msg().to_string();
            let result = self.context(msg);
            result.map_err(|e| warp::reject::custom(Error::from(e)))
        } else {
            let result = self;
            result.map_err(|e| warp::reject::custom(Error::from(e)))
        }
    }
}
