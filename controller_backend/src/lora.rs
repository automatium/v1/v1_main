use crate::serial::Serial;
use base64;
use failure::{err_msg, Error, ResultExt};
use log::{debug, info, trace};
use serde::{Deserialize, Serialize};
use std::env;

const PROTOCOL_VERSION: u8 = 0;
const LORA_MSG_MAX_LEN: usize = 50;

/// Interface to the LoRa modem
///
/// Let's say the client transmits once every hour.  It is very important we get those transmissions.
/// But how will we get them while transmitting?  Simple, we don't transmit except in response to a client.
/// So we spend most of our time in rx_wait, waiting for a message to come in.
/// When a message comes in, the application logic decides how to respond, if at all
pub struct LoRa {
    serial: Serial,
    address: u8,
}

impl LoRa {
    /// Create a new instance, auto-detecting the serial port
    pub fn from_auto() -> Result<Self, Error> {
        trace!("LoRa::from_auto");

        let serial = match env::var("LORA_MODEM_SERIAL") {
            Ok(port_name) => {
                info!("Connecting to LoRa modem at {}", port_name);
                Serial::from_port_name(&port_name)?
            }
            Err(e) => {
                debug!(
                    "Couldn't read from LORA_MODEM_SERIAL environment variable. {:?}",
                    e
                );
                info!("Auto-detecting LoRa modem");
                Serial::from_auto()?
            }
        };

        let mut new_self = Self {
            serial,
            // NOTE: setting the address has no effect in the current protocol version.
            address: 1,
        };


        let version = new_self
            .read_version()
            .context("Fetching protocol version")?;
        if version != PROTOCOL_VERSION {
            return Err(err_msg(
                "Modem's protocol version does not match what's installed on the Moteino!",
            ));
        }

        let init_ok = new_self
            .read_init_status()
            .context("Fetching init status")?;
        if !init_ok {
            return Err(err_msg("Modem did not initialize properly"));
        }

        Ok(new_self)
    }

    /// Wait for a packet to be received, ack it, and return the result
    /// timeout is in milliseconds
    /// Ok(None) will be returned if no message is received
    /// Err() will be returned for other errors
    pub fn rx_wait_ack(&mut self, timeout: u16) -> Result<Option<IncomingLoraMsg>, Error> {
        // validate the timeout
        let timeout_expanded = u128::from(timeout);
        let serial_timeout = self.serial.timeout().as_millis();
        if timeout_expanded > serial_timeout {
            return Err(err_msg(format!(
                "RX timeout ({}ms) must be shorter than serial timeout ({}ms).",
                timeout_expanded, serial_timeout
            )));
        }

        // send the message
        let request = RxWait { timeout };
        let mut response: Result<IncomingLoraMsg, ()> = self.serial.try_fetch_json(&request)?;

        match response {
            Ok(mut incoming_msg) => {
                // base64
                let bytes = base64::decode(&incoming_msg.data).context("decoding base64")?;
                incoming_msg.data = String::from_utf8(bytes).context("parsing utf-8")?;
                Ok(Some(incoming_msg))
            }
            Err(_e) => Ok(None),
        }
    }

    /// Send a packet and wait for an ACK
    pub fn send_wait(&mut self, dst: u8, msg: String) -> Result<(), Error> {
        // sanity check the outgoing message
        if msg.len() > LORA_MSG_MAX_LEN {
            return Err(err_msg(
                "Outgoing message is too long!  Max length is 50 chars.",
            ));
        }
        if dst == self.address {
            return Err(err_msg("Dst address is the same as src address."));
        }

        let request = SendPacket(OutgoingLoraMsg {
            dst,
            len: msg.len() as u8,
            data: msg,
        });
        let msg_was_sent: u8 = self.serial.fetch_json(&request)?;

        if msg_was_sent == 0 {
            Err(err_msg(
                "Error from RadioHead when sending the packet (probably a timeout)",
            ))
        } else {
            Ok(())
        }
    }

    /// Read the serial protocol version in use by the modem
    pub fn read_version(&mut self) -> Result<u8, Error> {
        let version: u8 = self.serial.fetch_json(&Version)?;

        Ok(version)
    }

    /// Check if the modem's radio initialized okay
    pub fn read_init_status(&mut self) -> Result<bool, Error> {
        let init_status: u8 = self.serial.fetch_json(&InitStatus)?;

        if init_status == 0 {
            Ok(false)
        } else {
            Ok(true)
        }
    }
}

#[derive(Serialize, Debug)]
pub struct OutgoingLoraMsg {
    dst: u8,
    len: u8,
    data: String,
}

#[derive(Deserialize, Debug)]
pub struct IncomingLoraMsg {
    src: u8,
    dst: u8,
    len: u8,
    data: String,
    id: u8,
    flags: u8,
}

#[derive(Serialize, Debug)]
#[serde(tag = "type")]
enum SerialRequest {
    // get the serial protocol version
    Version,
    // was the radio able to initialize without issues?
    InitStatus,
    // Wait up to timeout milliseconds for a message to come in
    // Return as soon as
    RxWait { timeout: u16 },
    // Send a packet, without waiting
    SendPacket(OutgoingLoraMsg),
}
use SerialRequest::*;